lazy val commonSettings = Seq(
  version := "0.1-SNAPSHOT",
  name := "camunda-test",
  organization := "com.camunda.training",
  scalaVersion := "2.13.0",
  fork := true,
  excludeDependencies ++= Seq(
    ExclusionRule("javax.activation", "activation")
  )
)

lazy val assemblySettings = Seq(
  assemblyMergeStrategy in assembly := {
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith "module-info.class" => MergeStrategy.discard
    case x: Any => (assemblyMergeStrategy in assembly).value(x)
  }
)

lazy val Main = Project("main", file("main"))
  .settings(commonSettings: _*)
  .settings(assemblySettings: _*)
  .enablePlugins(sbtassembly.AssemblyPlugin)
  .settings(
    libraryDependencies ++= Seq(
      "org.camunda.bpm" % "camunda-engine" % "7.10.0",
      "com.h2database" % "h2" % "1.4.199"
    ),
    mainClass in assembly := Some("com.camunda.training.Main"),
    assemblyJarName in assembly := "service.jar",
    assemblyOutputPath in assembly := file("dist/service.jar"),
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = true)
  )
