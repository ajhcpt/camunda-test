package com.camunda.training

import org.camunda.bpm.engine.ProcessEngineConfiguration

object Main extends App {
  val processEngineConfiguration = ProcessEngineConfiguration.createStandaloneInMemProcessEngineConfiguration()
  val processEngine = processEngineConfiguration.buildProcessEngine()
}
